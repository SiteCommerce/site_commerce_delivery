<?php

namespace Drupal\site_commerce_delivery\Plugin\SiteCommerce\DeliveryType;

use Drupal\site_commerce_delivery\DeliveryTypePluginBase;

/**
 * @DeliveryType(
 *   id = "site_commerce_delivery_type_pickup",
 *   label = @Translation("Pickup"),
 *   code = "pickup"
 * )
 */
class TypePickup extends DeliveryTypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    $config = \Drupal::config('site_commerce_delivery.type_pickup.settings');
    return [
      'allow_select' => (int) $config->get('allow_select'),
      'description' => (int) $config->get('description'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus() {
    $settings = $this->getSettings();
    return (bool) $settings['allow_select'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $settings = $this->getSettings();
    return $settings['description'];
  }

}
