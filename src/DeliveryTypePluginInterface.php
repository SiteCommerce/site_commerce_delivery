<?php

namespace Drupal\site_commerce_delivery;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface DeliveryTypePluginInterface extends PluginInspectionInterface {

  /**
   * Метод, который возвращает ID плагина.
   */
  public function getId();

  /**
   * Метод, который возвращает название плагина.
   */
  public function getLabel();

  /**
   * Метод, который возвращает объект с настройками плагина.
   */
  public function getSettings();

  /**
   * Метод, который возвращает машинный код способа доставки.
   */
  public function getCode();

  /**
   * Метод, который возвращает описание способа доставки.
   */
  public function getDescription();

  /**
   * Метод, который возвращает TRUE - если способ доставки доступен для выбора пользователю.
   */
  public function getAvailableStatus();

}
