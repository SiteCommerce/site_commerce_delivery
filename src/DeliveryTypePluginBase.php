<?php

namespace Drupal\site_commerce_delivery;

use Drupal\Component\Plugin\PluginBase;

abstract class DeliveryTypePluginBase extends PluginBase implements DeliveryTypePluginInterface {

  /**
   * Конструктор плагина.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCode() {
    return $this->pluginDefinition['code'];
  }

}
