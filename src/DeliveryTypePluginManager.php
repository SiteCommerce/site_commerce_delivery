<?php

namespace Drupal\site_commerce_delivery;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Менеджер плагина DeliveryPluginManager.
 */
class DeliveryTypePluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteCommerce/DeliveryType',
      $namespaces,
      $module_handler,
      'Drupal\site_commerce_delivery\DeliveryTypePluginInterface',
      'Drupal\site_commerce_delivery\Annotation\DeliveryType'
    );

    # Регистрируем hook_site_commerce_delivery_info_alter();
    $this->alterInfo('site_commerce_delivery_type_info');

    # Задаем ключ для кэша плагинов.
    $this->setCacheBackend($cache_backend, 'site_commerce_delivery_type');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

}
