<?php

namespace Drupal\site_commerce_delivery\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Form of TypeСourier plugin settings.
 */
class TypeСourierSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_commerce_delivery.type_courier.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_commerce_delivery_type_courier_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Настройки.
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    // Разрешить выбор способа доставки.
    $form['settings']['allow_select'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow selection of delivery method'),
      '#default_value' => $config->get('allow_select'),
    ];

    // Описание способа доставки.
    $form['settings']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description of the delivery method'),
      '#default_value' => $config->get('description'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Записывает значения в конфигурацию.
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $config->set('allow_select', (int) $form_state->getValue('allow_select'));
    $config->set('description', trim($form_state->getValue('description')));
    $config->save();

    parent::submitForm($form, $form_state);
  }
}
